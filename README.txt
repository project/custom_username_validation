Description
-----------

A pattern can be provided to validate the username on registration/user edit by
a regexp. Just copy module into you module folder and change settings at
"admin/config/people/custom_username_validation". For the pattern format, see
PHP's preg_match().

Other modules can make use of the validation as well using the function:
custom_username_validation_validate_username($username)


Permissions
-----------

administer custom username validation
Perform administration tasks for custom username validation.

override custom username validation
Users with this permission may override the custom username validation


Optionally force username change if invalid
-------------------------------------------

There is a checkbox to enable this functionality in the settings. It redirects
users back to their user edit page until they give a correct username unless:
- they have the "override custom username validation" permission
- they do not have the "change own username" permission (set this first for
  applicable roles).


Give admin the option to use custom username validation
-------------------------------------------------------

There is a checkbox to the module's configuration page. Unchecking the box lets
the admin use the pattern on "admin/people/create". Users who override username
validation will see the hint text of the pattern after the normal username-field
description.


Original author
---------------

Stefan T. Oertel <so AT oertel DOT it>
